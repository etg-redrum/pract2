use rand::Rng;

const TARGET_NUMBER: u32 = 10;

fn main() {
    let mut rng = rand::thread_rng();
    let random_number = rng.gen_range(1..=10);

    if random_number == TARGET_NUMBER {
        println!("Успешно!");
    } else {
        println!("Не успешно, попробуй позже");
    }
}
