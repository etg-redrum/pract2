FROM rust:latest
WORKDIR /usr/src/pract2
COPY . .

RUN cargo build --release

CMD ["./target/release/pract2"]
